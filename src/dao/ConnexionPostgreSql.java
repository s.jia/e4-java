package dao;
import java.sql.*;

public class ConnexionPostgreSql {
	
	private static Connection connect;
	//private static String url = "jdbc:postgresql://172.16.0.57:5432/s.jia";
	private static String url = "jdbc:postgresql://www.bts-malraux72.net:5432/s.jia";
	private static String user = "s.jia";
	private static String passwd = "P@ssword";

	public static Connection getInstance(){
		if(connect == null){
			try {
				Class.forName("org.postgresql.Driver");
				 connect = DriverManager.getConnection(url, user, passwd);
				} 			
			catch (SQLException e) {
					e.printStackTrace();
				} 
			catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		return connect;
		}

	public static void Arreter() {
		try {
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	

}
