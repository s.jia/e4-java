package classes;
public class Licencie {
	
	private String code, nom, prenom, dateNaiss;
	private Club leClub;

	/**
	    * Permet d'obtenir le code d'un licenci�
	    * @return - le code du club sous forme de chaine de caract�res
	    */
	public String getCode() {
		return code;
	}

	/**
	    * Permet la modification le code d'un licenci�
	    * @param titre - le nouveau code d'un licenci� � affecter
	    */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	    * Permet d'obtenir le nom d'un licenci�
	    * @return - le nom du licenci� sous forme de chaine de caract�res
	    */
	public String getNom() {
		return nom;
	}

	/**
	    * Permet la modification le nom d'un licenci�
	    * @param titre - le nouveau nom d'un licenci� � affecter
	    */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	    * Permet d'obtenir le pr�nom du licenci� 
	    * @return - le pr�nom du licenci� sous forme de chaine de caract�res
	    */
	public String getPrenom() {
		return prenom;
	}


	/**
	    * Permet la modification le pr�nom d'un licenci�
	    * @param titre - le nouveau pr�nom d'un licenci� � affecter
	    */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	    * Permet d'obtenir le pr�nom du licenci� 
	    * @return - la date de naissance du licenci� sous forme de chaine de caract�res
	    */
	public String getDateNaiss() {
		return dateNaiss;
	}

	/**
	    * Permet la modification la date de naissance d'un licenci�
	    * @param titre - la nouvelle date de naissance d'un licenci� � affecter
	    */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	/**
	    * Permet d'obtenir le club du licenci� 
	    * @return - le club du licenci�  sous la classe Club
	    */
	public Club getLeClub() {
		return leClub;
	}

	/**
	    * Permet la modification le club d'un licenci� 
	    * @param titre - le nouveau club d'un licenci� � affecter
	    */
	public void setLeClub(Club leClub) {
		this.leClub = leClub;
	}

	/**
	    * Construit un Licencie - initialise avec des valeurs par d�faut les propri�t�s de la classe
	    */ 
	public Licencie() {
		this.code = "";
		this.nom = "";
		this.prenom = "";
		this.dateNaiss = "";
		this.leClub = new Club();
	}
	
	/**
	    * Construit un Film - initialise avec des valeurs par d�faut les propri�t�s de la classe
	    * @param code - alimente la propri�t� code
	    * @param nom - alimente la propri�t� nom
	    * @param prenom - alimente la propri�t� prenom
	    * @param dateNaiss - alimente la propri�t� dateNaiss
	    * @param leClub - alimente la propri�t� leClub
	    */

	public Licencie(String code, String nom, String prenom, String dateNaiss, Club leClub) {
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.leClub = leClub;
	}

	@Override
	public String toString() {
		return "Licencie code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss
				+ ", leClub=" + leClub + "";
	}
	
	

}

