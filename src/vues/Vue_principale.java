package vues;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import classes.Club;
import classes.Licencie;
import controleur.ControleurPrincipal;

import java.awt.FlowLayout;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Color;
import javax.swing.JTextField;

public class Vue_principale extends JFrame {

	protected ControleurPrincipal controleur;
	public JPanel contentPane, PanelEnregistrer;
	public JComboBox comboBoxListe;
	private final JLabel lblChoix = new JLabel("Choisir un club :");
	private JButton btnAjouter, btnQuitter, btnSupprimer, btnEnregistrer;
	public JTextPane txtpnFondLicencie; 
	public JLabel lblNom, lblInfoClub,lblPrenom, lblDateDeNaissance, lblAfficheInfoClub;
	public JTextField textNom, textPrenom,textdateNaiss;
	private JTextField textField;
	private JButton Supprimer;
	public JPanel PanelSupprimer;
	private JLabel lblCode;
	public JTextField textCode;
	/**
	 * Launch the application.
	 */


	/**
	 * Create the frame.
	 */
	public Vue_principale(ControleurPrincipal unControleur) {
		this.controleur=unControleur;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 523, 405);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		comboBoxListe = new JComboBox();
		comboBoxListe.setActionCommand("comboBox");
		comboBoxListe.addActionListener(controleur);		
		comboBoxListe.setBounds(172, 34, 270, 24);
		contentPane.add(comboBoxListe);
		
		btnAjouter = new JButton("Ajouter");
		btnAjouter.setActionCommand("Ajouter");
		btnAjouter.addActionListener(controleur);
		btnAjouter.setBounds(42, 337, 117, 24);
		contentPane.add(btnAjouter);
		
		btnQuitter = new JButton("Quitter");		
		btnQuitter.setBounds(203, 336, 117, 25);
		btnQuitter.setActionCommand("Quitter");
		btnQuitter.addActionListener(controleur);
		contentPane.add(btnQuitter);
		
		btnSupprimer = new JButton("Supprimer");
		btnSupprimer.setActionCommand("Supprimer");
		btnSupprimer.addActionListener(controleur);
		btnSupprimer.setBounds(364, 337, 117, 25);
		contentPane.add(btnSupprimer);
		lblChoix.setBounds(12, 29, 125, 35);
		contentPane.add(lblChoix);
		
		lblInfoClub = new JLabel("Info du club : ");
		lblInfoClub.setBounds(12, 62, 125, 24);
		contentPane.add(lblInfoClub);
		
		txtpnFondLicencie = new JTextPane();
		txtpnFondLicencie.setBounds(12, 112, 499, 184);
		contentPane.add(txtpnFondLicencie);
		txtpnFondLicencie.setEditable(false);
		
		lblAfficheInfoClub = new JLabel("test");
		lblAfficheInfoClub.setBounds(113, 68, 396, 15);
		contentPane.add(lblAfficheInfoClub);

		txtpnFondLicencie.setVisible(false);
		
		PanelSupprimer = new JPanel();
		PanelSupprimer.setBounds(172, 95, 189, 90);
		contentPane.add(PanelSupprimer);
		PanelSupprimer.setVisible(false);
		
		lblCode = new JLabel("Code :");
		PanelSupprimer.add(lblCode);
		
		textCode = new JTextField();
		textCode.setColumns(10);
		PanelSupprimer.add(textCode);
		
		Supprimer = new JButton("Supprimer un licencié");
		Supprimer.setActionCommand("Supprimer un licencié");
		Supprimer.addActionListener(controleur);
		PanelSupprimer.add(Supprimer);
		
		PanelEnregistrer = new JPanel();
		PanelEnregistrer.setBackground(new Color(128, 128, 128));
		PanelEnregistrer.setBounds(100, 126, 342, 170);
		contentPane.add(PanelEnregistrer);
		PanelEnregistrer.setVisible(false);
		PanelEnregistrer.setLayout(null);
		lblPrenom = new JLabel("Prenom :");
		lblPrenom.setBounds(77, 42, 70, 15);
		PanelEnregistrer.add(lblPrenom);
		
		lblNom = new JLabel("Nom :");
		lblNom.setBounds(97, 69, 70, 15);
		PanelEnregistrer.add(lblNom);
		
		textNom = new JTextField();
		textNom.setBounds(147, 40, 114, 19);
		PanelEnregistrer.add(textNom);
		textNom.setColumns(10);
		
		textPrenom = new JTextField();
		textPrenom.setBounds(147, 65, 114, 19);
		PanelEnregistrer.add(textPrenom);
		textPrenom.setColumns(10);
		
		textdateNaiss = new JTextField();
		textdateNaiss.setBounds(167, 96, 114, 19);
		PanelEnregistrer.add(textdateNaiss);
		textdateNaiss.setColumns(10);
		
		btnEnregistrer = new JButton("Enregistrer");
		btnEnregistrer.setActionCommand("Enregistrer");
		btnEnregistrer.addActionListener(controleur);
		btnEnregistrer.setBounds(177, 127, 117, 25);
		PanelEnregistrer.add(btnEnregistrer);
		
		lblDateDeNaissance = new JLabel("Date de naissance :");
		lblDateDeNaissance.setBounds(12, 82, 207, 47);
		PanelEnregistrer.add(lblDateDeNaissance);
		lblAfficheInfoClub.setVisible(false);
	}
	
	public void remplirComboClub(List<Club> unclub) {
		comboBoxListe.addItem("-");
		for (Club c : unclub) {			
			comboBoxListe.addItem(c.getCode()+" - "+c.getNom());
		}
	}
	
	public void affichageInfoClub(Club c) {

		lblAfficheInfoClub.setVisible(true);
		lblAfficheInfoClub.setText(c.getNom()+" -Le nom de l'entraineur : "+c.getNom_entraineur()+" - Le nom du president : "+c.getNom_president());
	}
	
	public void afficher_licencie(List<Licencie> lesLicencies) {
		PanelEnregistrer.setVisible(false);
		PanelSupprimer.setVisible(false);
		txtpnFondLicencie.setVisible(true);
		txtpnFondLicencie.setText("");
		for (Licencie l : lesLicencies) {
			txtpnFondLicencie.setText(txtpnFondLicencie.getText()+l.toString());
		}
	}
	
	public void ajouter() {
		PanelEnregistrer.setVisible(true);
		txtpnFondLicencie.setVisible(false);
		PanelSupprimer.setVisible(false);
	}

				
	public void supprimer() {
		PanelSupprimer.setVisible(true);
		txtpnFondLicencie.setVisible(false);
		PanelEnregistrer.setVisible(false);
		
		
	}
}
