package tests;

import classes.Club;
import classes.Licencie;
import dao.ClubDAO;
import dao.DAO;
import dao.LicencieDAO;


public class TestsDAO {

	public static void main(String[] args) {
		
		// d�finition des objets d�acc�s aux donn�es
				//DAO<Licencie> licencie = new LicencieDAO();
				DAO<Club> club = new ClubDAO();
				/*
				// recherche d'un licenci� en fonction de son code (lecture / Read)
				System.out.println(licencie.read("TAGe").toString());
				*/
				// recherche d'un club en fonction de son code (lecture / Read)
				Club unClub = club.read("C41");
				
				/*	
				// insertion d'un nouveau licenci� (ajout / cr�ation / Create)
				Licencie unLicencie = new Licencie("CACl2", "CALVIN","Cl�mence","17/05/1990",
				unClub);
				licencie.create(unLicencie);
				
				// insertion d'un nouveau club (ajout / cr�ation / Create) .../...
				Club unClub = new Club("C41", "Mbap","KYKY","jp");
				club.create(unClub);
				
				// maj d�un licenci� (Update) .../...
				unLicencie.setNom("JIA");
				licencie.update(unLicencie);
				*/
				// maj d�un club (Update) .../...
				unClub.setNom("Bertrand");
				club.update(unClub);
				System.out.print(unClub);
				/*
				// r�cup�rer tous les clubs et les afficher .../...
				System.out.println(club.recupAll());
				
				// r�cup�rer tous les licenci�s et les afficher .../...
				System.out.println(licencie.recupAll());
				
				// suppression d�un licenci� (Delete) .../...
				licencie.delete(unLicencie);
				
				// suppression d�un club (Delete) .../...
				club.delete(unClub1);
			
			*/
	}

}
